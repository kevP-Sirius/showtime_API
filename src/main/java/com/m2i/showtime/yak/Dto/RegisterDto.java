package com.m2i.showtime.yak.Dto;

import lombok.Getter;

@Getter
public class RegisterDto {
    private String username;
    private String password;

}
