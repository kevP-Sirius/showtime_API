package com.m2i.showtime.yak.Dto;

public class ResetPasswordDto {
    private String username;
    private String password;
    private String passwordConfirm;
}

